<?php

include_once '../config/config.php'; //инициализация настроек
include_once '../config/db.php';   //инициализаия базы данных
include_once '../library/MainFunctions.php';  //основные функции

// определяем с каким контроллером будем работать
$controllerName = isset($_GET['controller']) ? ucfirst($_GET['controller']) : 'Index';

//определяем с какой функцией будем работатьs
$actionName = isset($_GET['action']) ? $_GET['action'] : 'index';


loadPage($smarty, $controllerName, $actionName);
