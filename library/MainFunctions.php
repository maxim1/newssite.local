<?php

/**
 * Формирование запрашиваемой страницы
 *
 * @param string $controllerName название контроллера
 * @param string $actionName название функции обработки страницы
 */

function loadPage($smarty, $controllerName, $actionName = 'index'){

    include_once PathPrefix.$controllerName.PathPostfix;

    $function = $actionName. 'Action';
    $function($smarty);
}

function loadTemplate($smarty, $templateName){

    $smarty->display($templateName . TemplatePostfix);
}

function d ($value =null, $die = 1){
    echo 'Debug: <br><pre>';
    print_r($value);
    echo '</pre>';
    if ($die) die;
}


/**
 * Функция получения ссылки да базу данных
 * @return mysqli ($db) - ссылка на базу данных
 */

function getDb (){
    global $db;
    return $db;
}


/**
 * Преобразование результата работы функции выборки в ассоциативный массив
 * @param $rs набор строк - результат работы SELECT
 * @return array|bool -
 */

function createSmartyRsArray ($rs){
    if (!$rs) return false;
    $smatryRs = array();

    while ($row = mysqli_fetch_assoc($rs)){

        $smatryRs [] = $row;
    }
    return $smatryRs;
}