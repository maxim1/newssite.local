<?php
/**
 * Контроллер страницы отображения полного текста новости
 */

//подключение моделей
include_once '../models/NewsModel.php';


/**
 * Формирование страницы с полным текстом новости
 * @param $smarty шаблонизатор
 */

function indexAction ($smarty){
    $newsId = isset($_GET['id']) ? $_GET['id'] :null;
    if($newsId==null) exit();
    $rsTextNews = null;

    $rsTextNews = getTextNewsById($newsId);

    $smarty->assign('pageTitle', $rsTextNews[0]['headlineNews']);
    $smarty->assign('rsTextNews', $rsTextNews);
    loadTemplate($smarty,'header');
    loadTemplate($smarty,'fullnews');
    loadTemplate($smarty,'footer');

}