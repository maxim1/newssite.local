<?php

//Подключение модели
include_once '../models/NewsModel.php';

function testAction(){
    echo 'IndexController.php > testAction';
}


function indexAction ($smarty){

    $rsNews = getAllMainNews();

    $smarty->assign('pageTitle', 'NewsSite.local - Сайт новостей');
    $smarty->assign('rsNews', $rsNews);

    loadTemplate($smarty,'header');
    loadTemplate($smarty,'index');
    loadTemplate($smarty,'footer');

}