<?php
/**
 * Модель для таблицы новостей
 */

/**
 * Получить заголовки всех новостей
 * @return array массив заголовков
 */

function getAllMainNews(){

    $sql = 'SELECT *
            FROM `news`
            ORDER BY `newsDate` DESC ';

    $rs = mysqli_query(getDb(),$sql);

    return createSmartyRsArray($rs);
}


/**
 *
 * * Выбор новости из базы по id
 *
 * @param $newsId  -  newsId id новости
 * @return array|bool
 */
function  getTextNewsById($newsId){

    $newsId = intval ($newsId);
    $sql = "SELECT *
            FROM `news`
            WHERE 
            id = '$newsId'";

    $rs = mysqli_query(getDb(),$sql);

    return createSmartyRsArray($rs);

}